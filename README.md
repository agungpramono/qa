 Robot MIS Lending End2End
## How to use
git clone git@bitbucket.org:Amartha/robot-lending-end2end.git
cd robot-dashboard-end2end

pip install robotframework
pip install robotframework-selenium2library
## Install Chromedriver
Download chromedriver latest version on this [link](https://chromedriver.chromium.org/)
place it in this path
/usr/local/bin/
## How to run - Single Execution By Testcase
robot -t {testcase_name} testCases/{file_name}.robot
## How to run - Single Execution By Module
robot testCases/{file_name}.robot
## How to run - Regression
1. Put all test cases in regression.robot
2. Run command
robot -A regression.robot
## Documentation for the library used
`
nb : This library doesn’t need to be declared in the code because it has built-in robotframework
`
* [OperatingSystem](https://robotframework.org/robotframework/latest/libraries/OperatingSystem.html#Create%20File)
* [BuiltIn](https://robotframework.org/robotframework/latest/libraries/BuiltIn.html)
* [String](http://robotframework.org/robotframework/2.8.7/libraries/String.html#Remove%20String)
`
nb : This library must be declared in code
`
* [selenium2library](http://robotframework.org/Selenium2Library/Selenium2Library.html#Xpath%20Should%20Match%20X%20Times)
### NB : Ask any QA Engineer for ENV